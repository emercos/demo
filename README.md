### Project to illustrate Kubernetes CRD / JKube problem

Be aware that I am in parallel trying to get <i>let's encrypt</i> to work, and as is now, it is not working. 
However for illustrating the jkube problem, its sufficient to run <pre>mvn clean install k8s:resource</pre>

### TRAEFIK
Traefik is installed from k8s/traefik23. Remark the CRDs in 001-rbac.yaml.  <pre> kubectl apply -f k8s/traefik23</pre>

### Kubectl
The demo application installs on kubernetes by running <pre>kubectl apply -f k8s/demo</pre>

### Jenkins
Install bítbucket plugin in jenkins
in bitbucket repositories/demo project find webhook and add url: https://jenkins.quasifact.com/bitbucket-hook/



## TEST

