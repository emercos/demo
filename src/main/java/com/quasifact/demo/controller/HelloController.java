package com.quasifact.demo.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String home() {
        log.debug("Calling hello");
        return "Hello kubernetes world - build on jenkins";
    }
}

